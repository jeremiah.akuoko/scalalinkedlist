
import scala.collection.LinearSeq

case class ::[+A](override val head: A, override val tail: LList[A]) extends LList[A] {

  override def apply(index: Int): A = if (index == 0) head else tail(index - 1)

  override def isEmpty: Boolean = false

  override def length: Int = 1 + tail.length
}

object LNil extends LList[Nothing] {
  override def isEmpty: Boolean = true
  override def length: Int = 0
  override val head: Nothing = throw new UnsupportedOperationException("Can't get the head of LNil")
  override val tail: Nothing = throw new UnsupportedOperationException("Can't get the head of LNil")
  override def apply(index: Int): Nothing = throw new UnsupportedOperationException("Can't index LNil")
}

sealed trait LList[+A] extends LinearSeq[A]{
//  val head: A
//  val tail: LList[A]

  //  /**
  //   * Gets element at index
  //   *
  //   * @param index the position of the element
  //   * @return
  //   */
  //  def get(index: Int): A

  /**
   * Determines whether the list is empty
   *
   * @return
   */
  def isEmpty: Boolean

  /**
   * Gets the length of the list
   *
   * @return
   */
  def length: Int

  /**
   * Adds am element to the front of the list
   */
  def ::[B >: A](a: B): LList[B] = new ::(a, this)

  /**
   * Concatenates two lists into a whole
   */
  //  def ++[B >: A](l1: LList[B]): LList[B]

  override def toString: String = super.toString
}

object LList {
  def apply[A](items: A*): LList[A] = {
    println(items)
    if (items.isEmpty) {
      println("Is empty")
      LNil
    }
    else {
      ::(items.head, apply(items.tail: _*))
    }
  }
}
