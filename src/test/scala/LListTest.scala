import munit.FunSuite

class LListTest extends FunSuite {
  //    val items = List(('a',5), ('b',3)).map((t) => (for(i <- 0 until t._2) yield t._1))
  //
  //    println(items.flatten)


  //  test("Adding an element to an empty list should return a list with just that element") {
  //    assertEquals(1 :: Empty, LList(1))
  //  }

  test("Creates an empty list") {
    val list = LList(32, 44)
    assertEquals(list.isEmpty, false)
  }

  test("Creates an empty list has length zero"){
    val list = LList()
    assertEquals(list.length, 0)
  }

  test("Getting element at index returns correct value"){
    val list = LList("Kofi", "Sammy", "Ama")

    case class Dog(name: String)

    val puppy = Dog("Puppy")
    val vallie = Dog("Vallie")
    val pesi = Dog("Pesi Boo")

    val dogs = LList(puppy, vallie, pesi)

    println(list)

//    assertEquals(dogs(2), pesi)
//    assertEquals(dogs.get(1).name, "Vallie")
//
//    assertEquals(list.get(2), "Ama")
//    assertEquals(list.get(0), "Kofi")
//    assertEquals(list.get(1), "Sammy")
  }
}
